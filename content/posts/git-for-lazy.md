Title: A git command for lazy people
Author: Lorenzo
Date: 2023-10-08
Category: Blog
Tags: blog, git
Slug: git-for-lazy

Sometimes you make small changes, e.g. to a configuration file. Other times you are maybe udating files (like in the case of a Pelican theme) which you need to push to trigger a pipeline and publish.

In any case if one is feeling lazy in a good, [Larry Wall](https://en.wikiquote.org/wiki/Larry_Wall#Other) style, you could have a simple bash script like this.

```
#!/bin/bash
if [ -z "$2" ]
then
	$COMMIT_COMMAND="-e"
else
	$COMMIT_COMMAND="-m $2"
fi
git add $1 && git commit $COMMIT_COMMAND && git push origin main
```
This still alows to control which files to add (via the `$1` argument) and also decide if to add the commit message or get an interactive one.

Like with many lazy things, the scripts assumes one would push to origin main... But this could also be configured.

There are more refined approaches using [git aliases](https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases) etc. but this is a quick and... lazy script which works.