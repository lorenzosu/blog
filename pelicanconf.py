#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Lorenzo'
SITENAME = 'lorenzosu blog'
SITEURL = 'lorenzosu.gitlab.io'

PATH = 'content'
THEME = './theme/notmyidea_mod'
PLUGIN_PATHS = ['plugins']

JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGINS = ['i18n_subsites']
STATIC_PATHS = [
    'images',
    'extras'
    ]
EXTRA_PATH_METADATA = {
    'extras/favicon.ico': {'path': 'favicon.ico'},
}

TIMEZONE = 'Europe/Rome'
DEFAULT_LANG = 'en'

LOAD_CONTENT_CACHE = False

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ('Gitlab', 'https://gitlab.com/lorenzosu'),
    ('Webspace', 'https://lorenzosu.net')
    )

# Social widget
SOCIAL = (
    ('SoundCloud', 'https://soundcloud.com/lorenzosu'),
    ('YouTube', 'https://www.youtube.com/channel/UCYw1TLlip416Sg6InDxudmQ')
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False
